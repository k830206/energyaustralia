"""Test Case for MotorCar class"""
import os
from unittest import TestCase
from unittest.mock import patch
from src.motor_car import MotorCar


class TestMotorCar(TestCase):
    """Test Case class definition"""
    def setUp(self):
        self.motor_car = MotorCar()

    def get_car_info(self, file_name):
        """Mock for normal server responses with pre-defined files"""
        print(os.path.abspath('payloads/%s' % file_name))
        with open('tests/payloads/%s' % file_name, 'r') as file_obj:
            json_str = file_obj.read()

        with patch('requests.request') as mock_request:
            mock_request.return_value.status_code = 200
            mock_request.return_value.text = json_str

            car_info = self.motor_car.get_car_info()

        return car_info

    @staticmethod
    def get_expected_output(file_name):
        """Load output strings from a file"""
        with open('tests/expected/%s' % file_name, 'r') as file_obj:
            output_txt = file_obj.read()

        return output_txt

    def test_car_info_with_blank_content(self):
        """Test for blank content with status code 200"""
        with patch('requests.request') as mock_request:
            mock_request.return_value.status_code = 200
            mock_request.return_value.text = ''

            car_info = self.motor_car.get_car_info()
            self.assertEqual(car_info, None, 'It should return None when it has a blank context')

    def test_car_info_with_error_response(self):
        """Test for blank content with status code 400"""
        with patch('requests.request') as mock_request:
            mock_request.return_value.status_code = 400
            mock_request.return_value.reason = 'Bad Request'

            car_info = self.motor_car.get_car_info()
            self.assertEqual(car_info, None, 'It should return None when it has an error response')

    def test_print_with_full_content(self):
        """Test for full content"""
        car_info = self.get_car_info('sample_response.json')
        output_list = self.motor_car.print_info(car_info)

        self.assertEqual(0,
                         len(list(filter(lambda x: x.lower().strip() == 'unknown', output_list))),
                         'It should have no unknown lines')

        expected_txt = self.get_expected_output('print_full_content.txt')
        actual_txt = os.linesep.join(output_list)

        self.assertEqual(expected_txt, actual_txt, 'Output text is NOT matched')

    def test_print_with_blank_fields(self):
        """Test for full content with blank fields and duplicated fields"""
        car_info = self.get_car_info('blank_show_name.json')
        output_list = self.motor_car.print_info(car_info)

        unknown_count = sum(
            map(
                lambda line: len(list(
                    filter(lambda word: str(word).lower().strip() == 'unknown', line.split(' '))
                )), output_list))

        self.assertEqual(3, unknown_count, 'It should have 3 unknown lines')

        expected_txt = self.get_expected_output('print_with_blank_content.txt')
        actual_txt = os.linesep.join(output_list)

        self.assertEqual(expected_txt, actual_txt, 'Output text is NOT matched')
