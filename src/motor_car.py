"""MotorCar class implementation: get data from API then group by make, model, show_name"""

import os
import logging
import json
import time
from itertools import groupby
from collections import OrderedDict
import requests
from src.config import Config
from src.messages import EMPTY_RESPONSE, FAIL_TO_GET_DATA, CONFIGURATION_NOT_FOUND, \
    NO_CAR_INFO, UNKNOWN


class MotorCar:
    """MotorCar class: load configuration file when it is initialized"""
    CONFIG_FILE_NAME = 'config.json'
    config = dict()

    def __init__(self):
        self.__load_config_file__()

    def __load_config_file__(self):
        if not os.path.exists(self.CONFIG_FILE_NAME):
            logging.error(CONFIGURATION_NOT_FOUND, self.CONFIG_FILE_NAME)
        else:
            with open(self.CONFIG_FILE_NAME, 'r') as file_obj:
                self.config = self.__get_config_obj__(json.load(file_obj))

    @staticmethod
    def __get_config_obj__(config_json):
        return Config(config_json['url'], config_json['method'], config_json['headers'])

    @staticmethod
    def combine_info(car_info, car_show):
        """Combine information to make a flatten list"""
        data_row = {
            'make': car_info['make'] if 'make' in car_info.keys() else UNKNOWN,
            'model': car_info['model'] if 'model' in car_info.keys() else UNKNOWN,
            'name': car_show
        }

        for data_key, data_value in data_row.items():
            if not data_value.strip():
                data_row[data_key] = UNKNOWN

        return data_row

    def get_car_info(self):
        """Get a car information based on sorted and groupby processes"""
        res = requests.request(self.config.method, self.config.url, headers=self.config.headers)

        if res.status_code == 200:
            if (res.text is not None) and (len(res.text) > 2):
                logging.debug('Start to process')
                car_show_list = json.loads(res.text)
                flat_list = list()

                for car_show in car_show_list:
                    show_name = car_show['name'] if 'name' in car_show.keys() else UNKNOWN
                    cars = car_show['cars'] if 'cars' in car_show.keys() else list()

                    flat_list.extend(list(
                        map(lambda car_info: self.combine_info(car_info, show_name), cars)))

                flat_list = sorted(flat_list, key=lambda x: (x['make'], x['model'], x['name']))

                summerized_dict = OrderedDict()

                for (car_make, car_detail) in groupby(flat_list, lambda x: x['make']):
                    summerized_dict[car_make] = OrderedDict()

                    for (car_model, shows) in groupby(list(car_detail), lambda x: x['model']):
                        summerized_dict[car_make][car_model] = \
                            [key for key, group in
                             groupby(list(map(lambda x: x['name'], list(shows))))]

                logging.debug(res.text)
                logging.debug(str(summerized_dict))
                logging.debug('Complete to process')

                return summerized_dict
            else:
                logging.warning(EMPTY_RESPONSE)
        else:
            logging.warning(FAIL_TO_GET_DATA, res.status_code, str(res.reason))

        return None

    def get_car_info_with_retry(self, retry_count=5):
        """Get a car information with several retries"""
        num_of_try = 0

        while num_of_try < retry_count:
            logging.info('Try to gather car information [%d/%d]', (num_of_try + 1), retry_count)
            car_info = self.get_car_info()

            if car_info is not None:
                return car_info
            else:
                num_of_try = num_of_try + 1
                time.sleep(1)

        return None

    @staticmethod
    def print_info(car_info=None):
        """Print car information through standard output"""
        output_list = list()

        if car_info is not None:
            for car_maker, car_models in car_info.items():
                output_list.append(car_maker)

                for car_model, motor_shows in car_models.items():
                    output_list.append('\t%s' % car_model)

                    output_list.extend(list(map(
                        lambda motor_show: '\t\t%s' % motor_show,
                        filter(lambda x: x.strip(), motor_shows))))

            logging.debug(
                'Car Information ===================%s%s', os.linesep, os.linesep.join(output_list))
        else:
            logging.warning(NO_CAR_INFO)

        return output_list
