"""Configuration domain object"""
import json


class Config:
    """EnergyAustralia's API Configuration Class"""
    def __init__(self, url, method, headers=None):
        self.url = url
        self.method = method
        self.headers = dict() if headers is None else headers

    def __str__(self):
        return json.dumps({'url': self.url, 'method': self.method, 'headers': self.headers})
