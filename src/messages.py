"""A file to provide message formats"""

EMPTY_RESPONSE = 'Empty response from the server'
FAIL_TO_GET_DATA = 'Fail to get data from the server: [%d] %s'
CONFIGURATION_NOT_FOUND = 'CANNOT find a configuration file: %s'
NO_CAR_INFO = 'There is no information. It might be better warning messages ' \
              'which are displayed above'
UNKNOWN = 'Unknown'
