#!/usr/bin/env bash
python -m virtualenv motorcar --python=$(which python3)
source motorcar/bin/activate
pip install -r requirements.txt

# Check a code quality: Linting framework
pylint main.py
pylint src/*.py
pylint tests/*.py

# Unit test
python -m unittest tests/test_motor_car.py

# Run a main module
python main.py