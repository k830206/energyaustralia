"""Main Runner"""
import os
import logging
from src.motor_car import MotorCar


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    MOTOR_CAR = MotorCar()
    CAR_INFO = MOTOR_CAR.get_car_info_with_retry()
    OUTPUT_LINES = MOTOR_CAR.print_info(CAR_INFO)

    print(os.linesep.join(OUTPUT_LINES))
