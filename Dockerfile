FROM python:3.7.2-stretch

WORKDIR energy

COPY *.py ./
COPY *.sh ./
COPY requirements.txt requirements.txt
COPY config.json config.json
COPY src/*.* src/
COPY tests/*.* tests/

RUN ["pip3", "install", "virtualenv"]
RUN ["chmod", "+x", "run-in-local.sh"]

ENTRYPOINT "./run-in-local.sh"
