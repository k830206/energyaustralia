### How to run

##### Run in Local
_Make sure that permission for those two files should be set before you start_

_It supports on Python 3.7.x or above environment_

1. Set up an environment: `./setup.sh`

2. Run a program: `./run-in-local.sh`
       
##### Run in Docker
In case that Python is not installed in your environment, you can run it through Docker container

Run a program: `./run-in-docker.sh`



