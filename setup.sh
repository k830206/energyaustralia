#!/usr/bin/env bash

IS_PYTHON_INSTALLED=$(command -v python)

if [[ "${IS_PYTHON_INSTALLED}" = "" ]]
then
    echo "Install Python first!"
    exit 1
fi

PY_VERSION=$(python -c "import sys; print(sys.version[0]);")

if [[ ${PY_VERSION} = 2 ]]
then
    PIP_COMMAND="pip"
else
    PIP_COMMAND="pip3"
fi

IS_PIP_INSTALLED=$(command -v ${PIP_COMMAND})

if [[ "${IS_PIP_INSTALLED}" = "" ]]
then
    echo "Try to install pip"
    sudo easy_install pip
    echo "Complete to install pip"
fi

echo "Try to install virtualenv"
sudo python -m ${PIP_COMMAND} install --upgrade pip
sudo python -m ${PIP_COMMAND} install virtualenv --upgrade
echo "Complete to install virtualenv"